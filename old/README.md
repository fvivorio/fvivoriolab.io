# fvivorio.gitlab.io

My portfolio website 🚀️

##This website was entirely built using free and/or open source software. So thank you everyone who made this possible:

 - Ubuntu 20.04 LTS - GNU/Linux Operating System by Canonical, Ltd. (http://www.ubuntu.com/)
 - Inkscape - vector graphics editor (http://www.inkscape.org/)
 - Brackets - modern text editor for web development by Adobe Systems Incorporated (http://www.adobe.com - http://brackets.io)
 - Figma - vector graphics editor and UI/UX protityping tool (https://www.figma.com)
 - Site.js - small Web construction set (https://sitejs.org/)
 - GitLab - Git-repository hosting service (https://gitlab.com/)
 - Git - revision control system powered by Linux (https://git-scm.com/)
 - SVGOMG! and SVGO - SVG Optimizer is a Nodejs-based tool for optimizing SVG vector graphics files (https://github.com/jakearchibald/svgomg)

>Thank you so much for this beatiful blog who helped me building it: Codrops - web design and development blog (https://tympanus.net/codrops/)
At least but not less important: thank you for MDN and Firefox team - Mozilla Development Network - I've learnt a LOT of things there (https://developer.mozilla.org/)

Visit it here: https://fvivorio.gitlab.io